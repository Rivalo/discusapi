package main

import (
	"fmt"

	"github.com/codegangsta/negroni"      // Good and Fast middleware support
	"github.com/julienschmidt/httprouter" // Fast router
)

func main() {
	router := httprouter.New()
	router.GET("/nu", Nunl)
	router.GET("/test/:name", test)

	fmt.Println(Sum(1, 2))

	n := negroni.Classic()
	n.UseHandler(router)
	n.Run(":3000")
}
