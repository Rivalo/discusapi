package main

import (
	"fmt"
	"net/http"

	"github.com/Rivalo/nu"
	"github.com/julienschmidt/httprouter"
)

//Nunl prints JSON from nu.nl RSS feed
func Nunl(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	JSON, err := nu.JSON("http://www.nu.nl/rss/Algemeen")
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Fprint(w, string(JSON))
}

func test(w http.ResponseWriter, r *http.Request, x httprouter.Params) {
	fmt.Println(x.ByName("name"))
}

//Sum the shit
func Sum(a, b int) int {
	return a + b
}
